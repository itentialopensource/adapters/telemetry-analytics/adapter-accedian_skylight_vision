## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Accedian Skylight Vision. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Accedian Skylight Vision.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Accedian_skylight_vision. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">searchNetworkElement(type, query, callback)</td>
    <td style="padding:15px">Search</td>
    <td style="padding:15px">{base_path}/{version}/Search/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listSearchObjects(callback)</td>
    <td style="padding:15px">List Search Objects</td>
    <td style="padding:15px">{base_path}/{version}/Search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchManagedObject(type, query, page, limit, callback)</td>
    <td style="padding:15px">Search Managed Object</td>
    <td style="padding:15px">{base_path}/{version}/Search/Inventory/managedObject/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchNetworkElements(limit, page, body, callback)</td>
    <td style="padding:15px">Search Network Elements</td>
    <td style="padding:15px">{base_path}/{version}/Search/Inventory/networkElements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemVersion(callback)</td>
    <td style="padding:15px">Version</td>
    <td style="padding:15px">{base_path}/{version}/System/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">keepalive(callback)</td>
    <td style="padding:15px">Keepalive</td>
    <td style="padding:15px">{base_path}/{version}/System/keepalive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTimezone(callback)</td>
    <td style="padding:15px">Timezone</td>
    <td style="padding:15px">{base_path}/{version}/System/time/timezone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTime(callback)</td>
    <td style="padding:15px">Get System Time</td>
    <td style="padding:15px">{base_path}/{version}/System/time?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTimezoneOffset(callback)</td>
    <td style="padding:15px">Get System Timezone Offset</td>
    <td style="padding:15px">{base_path}/{version}/System/time/timezone/offset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCLIScript(body, callback)</td>
    <td style="padding:15px">Create CLI Script</td>
    <td style="padding:15px">{base_path}/{version}/CliScript?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCLIScript(id, body, callback)</td>
    <td style="padding:15px">Update CLI Script</td>
    <td style="padding:15px">{base_path}/{version}/CliScript/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCLIScript(id, callback)</td>
    <td style="padding:15px">Get CLI Script</td>
    <td style="padding:15px">{base_path}/{version}/CliScript/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCLIScript(id, callback)</td>
    <td style="padding:15px">Delete CLI Script</td>
    <td style="padding:15px">{base_path}/{version}/CliScript/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setConfigFlowExecutorParam(id, body, callback)</td>
    <td style="padding:15px">Set Executor Token Param</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}/param?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startConfigFlowExecutor(id, callback)</td>
    <td style="padding:15px">Start Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopConfigFlowExecutor(id, callback)</td>
    <td style="padding:15px">Stop Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createConfigFlowExecutor(body, callback)</td>
    <td style="padding:15px">Create Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignConfigFlowExecutor(id, body, callback)</td>
    <td style="padding:15px">Assign Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateConfigFlowExecutor(id, body, callback)</td>
    <td style="padding:15px">Update Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFlowExecutor(id, callback)</td>
    <td style="padding:15px">Get Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteConfigFlowExecutor(id, callback)</td>
    <td style="padding:15px">Delete Config Flow Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowExecutor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigFlowProfile(id, callback)</td>
    <td style="padding:15px">Get Config Flow Profile</td>
    <td style="padding:15px">{base_path}/{version}/ConfigFlowProfile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigJob(id, callback)</td>
    <td style="padding:15px">Get Config Job</td>
    <td style="padding:15px">{base_path}/{version}/ConfigJob/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfigJobExecutor(id, callback)</td>
    <td style="padding:15px">Get Config Job Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigJobExecutor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startConfigJobExecutor(id, callback)</td>
    <td style="padding:15px">Start Config Job Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigJobExecutor/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopConfigJobExecutor(id, callback)</td>
    <td style="padding:15px">Stop Config Job Executor</td>
    <td style="padding:15px">{base_path}/{version}/ConfigJobExecutor/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setAuthenticationController(body, callback)</td>
    <td style="padding:15px">Set Authenication Configuration</td>
    <td style="padding:15px">{base_path}/{version}/Configuration/authenticationConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationConfiguration(callback)</td>
    <td style="padding:15px">Get Authenication Configuration</td>
    <td style="padding:15px">{base_path}/{version}/Configuration/authenticationConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataset(id, callback)</td>
    <td style="padding:15px">Get Dataset</td>
    <td style="padding:15px">{base_path}/{version}/Dataset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDataset(id, body, callback)</td>
    <td style="padding:15px">Update Dataset</td>
    <td style="padding:15px">{base_path}/{version}/Dataset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDataset(id, callback)</td>
    <td style="padding:15px">Delete Dataset</td>
    <td style="padding:15px">{base_path}/{version}/Dataset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDataset(body, callback)</td>
    <td style="padding:15px">Create Dataset</td>
    <td style="padding:15px">{base_path}/{version}/Dataset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEmbeddedManagedObject(type, id, embeddedType, embeddedId, callback)</td>
    <td style="padding:15px">Get Embedded Managed Object</td>
    <td style="padding:15px">{base_path}/{version}/Inventory/managedObject/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagedObjectByTypeId(type, id, callback)</td>
    <td style="padding:15px">Get Managed Object By Type and ID</td>
    <td style="padding:15px">{base_path}/{version}/Inventory/managedObject/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagedObjectByDN(dn, callback)</td>
    <td style="padding:15px">Get Managed Object By DN</td>
    <td style="padding:15px">{base_path}/{version}/Inventory/managedObject/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getManagedObjectNetworkElement(id, body, callback)</td>
    <td style="padding:15px">Get Managed Obect For Network Element</td>
    <td style="padding:15px">{base_path}/{version}/Inventory/networkElement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNECredential(id, body, callback)</td>
    <td style="padding:15px">Update NE Credential</td>
    <td style="padding:15px">{base_path}/{version}/NECredential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNECredential(id, callback)</td>
    <td style="padding:15px">Get NE Credential</td>
    <td style="padding:15px">{base_path}/{version}/NECredential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNECredential(id, callback)</td>
    <td style="padding:15px">Delete NE Credential</td>
    <td style="padding:15px">{base_path}/{version}/NECredential/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNECredential(body, callback)</td>
    <td style="padding:15px">Create NE Credential</td>
    <td style="padding:15px">{base_path}/{version}/NECredential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterface(id, callback)</td>
    <td style="padding:15px">Get Interface</td>
    <td style="padding:15px">{base_path}/{version}/NEInterface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPort(id, callback)</td>
    <td style="padding:15px">Get Port</td>
    <td style="padding:15px">{base_path}/{version}/NEPort/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendCommandToNetworkElement(id, body, callback)</td>
    <td style="padding:15px">Send Command to Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}/directCommand?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkElementResynch(id, callback)</td>
    <td style="padding:15px">Network Element Resynch</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}/resynch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addElementSubtendSerial(id, serial, callback)</td>
    <td style="padding:15px">Add an element that is in the element inventory</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}/addSubtend/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkElementInventory(id, callback)</td>
    <td style="padding:15px">Get Network Element Inventory</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}/inventory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manageNetworkElement(id, callback)</td>
    <td style="padding:15px">Manage Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}/manage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unmanageNetworkElement(id, callback)</td>
    <td style="padding:15px">Unmanage Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}/unmanage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkElement(id, callback)</td>
    <td style="padding:15px">Update Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkElement(id, callback)</td>
    <td style="padding:15px">Get Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkElement(id, callback)</td>
    <td style="padding:15px">Delete Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkElement(body, callback)</td>
    <td style="padding:15px">Create Network Element</td>
    <td style="padding:15px">{base_path}/{version}/NetworkElement?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRFC2544TestReport(id, callback)</td>
    <td style="padding:15px">Get RFC2544 test report</td>
    <td style="padding:15px">{base_path}/{version}/SAT/RFC2544/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRFC2544TestReport(id, callback)</td>
    <td style="padding:15px">Delete RFC2544 test report</td>
    <td style="padding:15px">{base_path}/{version}/SAT/RFC2544/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startRFC2544Test(body, callback)</td>
    <td style="padding:15px">Start RFC2544 Test</td>
    <td style="padding:15px">{base_path}/{version}/SAT/RFC2544/startTest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopRFC2544Test(id, callback)</td>
    <td style="padding:15px">Stop RFC2544 Test</td>
    <td style="padding:15px">{base_path}/{version}/SAT/RFC2544/{pathv1}/stopTest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getY1564TestReport(id, callback)</td>
    <td style="padding:15px">Get Y1564 Report</td>
    <td style="padding:15px">{base_path}/{version}/SAT/Y1564/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteY1564TestReport(id, callback)</td>
    <td style="padding:15px">Delete Y1564 Test Report</td>
    <td style="padding:15px">{base_path}/{version}/SAT/Y1564/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startY1564Test(body, callback)</td>
    <td style="padding:15px">Start Y1564 test</td>
    <td style="padding:15px">{base_path}/{version}/SAT/Y1564/startTest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopY1564Test(id, callback)</td>
    <td style="padding:15px">Stop Y1564 Test</td>
    <td style="padding:15px">{base_path}/{version}/SAT/Y1564/{pathv1}/stopTest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
