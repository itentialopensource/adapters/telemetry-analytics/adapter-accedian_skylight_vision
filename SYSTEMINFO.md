# Accedian Skylight Vision

Vendor: Accedian (acquired by Cisco)
Homepage: https://www.cisco.com/

Product: Accedian Skylight Vision
Product Page: https://www.cisco.com/site/us/en/products/networking/software/provider-connectivity-assurance/index.html

## Introduction
We classify Accedian Skylight Vision into the Service Assurance domain as Accedian Skylight Vision provides tools and endpoints for monitoring and managing network performance and configurations. 

"Managing networks has become increasingly complex; reducing operational expenses and increasing productivity become key strategies in maximizing shareholder value."

## Why Integrate
The Accedian Skylight Vision adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Accedian Skylight Director Vision. With this adapter you have the ability to perform operations such as:

- Execute CLI Script
- Get Config Flow Executor
- Create Config Flow Executor
- Get Inventory
- Create Network Element
- Execute RFC2544 Test
- Execute Y.1564 Test

## Additional Product Documentation
The [API documents for Accedian Skylight Suite](https://docs.accedian.io/docs/intro-to-rest-apis)