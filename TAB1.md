# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Accedian_skylight_vision System. The API that was used to build the adapter for Accedian_skylight_vision is usually available in the report directory of this adapter. The adapter utilizes the Accedian_skylight_vision API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Accedian Skylight Vision adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Accedian Skylight Director Vision. With this adapter you have the ability to perform operations such as:

- Execute CLI Script
- Get Config Flow Executor
- Create Config Flow Executor
- Get Inventory
- Create Network Element
- Execute RFC2544 Test
- Execute Y.1564 Test

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
