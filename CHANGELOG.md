
## 0.4.5 [10-15-2024]

* Changes made at 2024.10.14_20:56PM

See merge request itentialopensource/adapters/adapter-accedian_skylight_vision!16

---

## 0.4.4 [08-21-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-accedian_skylight_vision!14

---

## 0.4.3 [08-14-2024]

* Changes made at 2024.08.14_19:15PM

See merge request itentialopensource/adapters/adapter-accedian_skylight_vision!13

---

## 0.4.2 [08-07-2024]

* Changes made at 2024.08.06_20:50PM

See merge request itentialopensource/adapters/adapter-accedian_skylight_vision!12

---

## 0.4.1 [08-05-2024]

* Changes made at 2024.08.05_14:46PM

See merge request itentialopensource/adapters/adapter-accedian_skylight_vision!11

---

## 0.4.0 [07-03-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight_vision!10

---

## 0.3.3 [03-27-2024]

* Changes made at 2024.03.27_13:19PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight_vision!9

---

## 0.3.2 [03-12-2024]

* Changes made at 2024.03.12_11:02AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight_vision!8

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:37AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight_vision!7

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-accedian_skylight_vision!6

---
